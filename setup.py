from setuptools import setup

setup(
        name='cryptocurrency-cli-tools',
        version='0.0.3',
        description='CLI commands for common cryptocurrency related operations',
        author='Louis Holbrook',
        author_email='dev@holbrook.no',
        install_requires=[
            'web3==5.12.2',
            'bip_utils==1.4.0',
            ],
        scripts=[
            'scripts/bip39gen',
            'scripts/ethereum-checksum-address',
            ],
        url='https://gitlab.com/nolash/cryptocurrency-cli-tools',
        classifiers=[
            'Programming Language :: Python :: 3',
            'Operating System :: OS Independent',
            'Development Status :: 3 - Alpha',
            'Environment :: Console',
            'Intended Audience :: End Users/Desktop',
            'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
            'Topic :: Utilities',
            ],
        )
